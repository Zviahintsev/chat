const express = require('express');
const app =  express();
const fs = require('fs')
const express_graphql = require('express-graphql');
const { buildSchema } = require('graphql');
const cors = require('cors')
const Sequelize = require('sequelize');
const sequelize = new Sequelize('chat', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false
});
const ChatRoom = sequelize.define('chatroom', {chatroom : Sequelize.STRING });
const Message = sequelize.define('message', { message: Sequelize.STRING });
const Users = sequelize.define('users', {user:Sequelize.STRING});

// Here we can connect countries and cities base on country code
ChatRoom.hasMany(Message);
Message.belongsTo(ChatRoom);

ChatRoom.belongsTo(Users, { as: 'owner' });
ChatRoom.belongsToMany(Users, { through: 'userschatroom' });
Users.belongsToMany(ChatRoom, { through:'userschatroom'})

sequelize.sync()  
app.use(cors())
 
app.get('/products/:id', function (req, res, next) {
  res.json({msg: 'This is CORS-enabled for all origins!'})
})

app.post('/users/',async(reg, res)=>{
  let usr = Users.create({user:reg.body.username,
                          login:reg.body.login,
                          password:reg.body.password});
})

app.post("/login", async(reg, res)=>{
    let login = await Users.findOne({
         where: (username?password==true:alert('Access dinied'))
    })
})
app.post('/message', async (req, res) => {
  let msg = Message.create({name: req.body.name,
                           text: req.body.text,
                          chatroom:req.body._id});
  await msg.save()
  res.status(201);
  res.end(JSON.stringify({id: msg._id.toString()}))
})

app.get('/message/:id?', async (req, res) => {
  let { id }  = req.params
  if (id){
      let msg  = await Message.findById(id)
      res.end(JSON.stringify(msg))
  }
  else {
      let msgs = await Message.find().sort([['_id', 1]])
      res.end(JSON.stringify(msgs))
  }
})
app.post('/chatroom/:id?',async(reg, res) => {
  let cr = await ChatRoom.findById(req.params.chatroomId)
  msg = await cr.getMessages() 
})

var schema = buildSchema(`
    type Query {
        getMessages(id: Int!): Messages
        getMessages: [Messages]
        getUsers(id: Int!): [Users]
        
    }
    type Mutation {
        createMessages(message: String!, createdAt: String!, updatedAt:String!): Users
        createUsers(id: Int!, user: String!): Messages
    }

    type Messages {
        id: Int
        message: String
        createdAt:  String
        key: String
    }
    type Users {
        id: Int
        user:  String
    }
`);

var root = {
  getMessages,
  getUsers,
  createMessages,
  createUsers,
};



// Create an express server and a GraphQL endpoint
var app = express();
app.use(cors())

app.use('/graphql', express_graphql({
  schema: schema,
  rootValue: root,
  graphiql: true
}));

// app.listen(80, function () {
//   console.log('CORS-enabled web server listening on port 80')
// })

app.listen(3001, () => {
    console.log('Example appp listen on port 3001!');
});
